/*
 * Public API Surface of custom-pdf-annotation
 */

export * from './lib/custom-pdf-annotation.service';
export * from './lib/custom-pdf-annotation.component';
export * from './lib/custom-pdf-annotation.module';

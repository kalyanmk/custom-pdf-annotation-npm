import { TestBed } from '@angular/core/testing';

import { CustomPdfAnnotationService } from './custom-pdf-annotation.service';

describe('CustomPdfAnnotationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomPdfAnnotationService = TestBed.get(CustomPdfAnnotationService);
    expect(service).toBeTruthy();
  });
});

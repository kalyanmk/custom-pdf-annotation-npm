import { Component, OnInit, OnChanges, EventEmitter, Input, Output, KeyValueChanges, KeyValueDiffer, KeyValueDiffers, HostListener, SimpleChange, SimpleChanges, ElementRef, Renderer2, ViewEncapsulation } from '@angular/core';
declare var require: any;
let PDFJSAnnotate = require('src/assets/JS/pdf-annotate.js');
import { Tesseract } from 'tesseract.ts';
import cloneDeep from 'lodash/cloneDeep';
import { UUID } from 'angular2-uuid';

const { UI } = PDFJSAnnotate;
declare let PDFJS: any;
export interface Filters {
  "filter": string
}
export interface PDFconfig {
  "rendercirclesalongwithindex"?: boolean,
  "loadallpages": boolean,
  "zoomLevel"?: any,
  "rotate"?: any,
  "totalpages": number,
  "currentPage": number,
  "history_enabled"?: boolean,
  "showLabels"?: boolean,
  "history_count_per_annotation"?: number,
  "statusFilter"?: Array<Filters>,
  "deleteAlert"?: boolean,
  "useteseract"?: boolean
}

@Component({
  selector: 'enl-custom-pdf-annotation',
  templateUrl: './custom-pdf-annotation.component.html',
  styleUrls: ['./custom-pdf-annotation.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CustomPdfAnnotationComponent implements OnInit, OnChanges {

  @Output() pageChanged = new EventEmitter();
  @Output() annotationEdited = new EventEmitter();
  @Output() annotationAdded = new EventEmitter();
  @Output() annotationDeleted = new EventEmitter();
  @Output() captureData?= new EventEmitter();
  @Output() mouseLeaveFrmPDF = new EventEmitter();
  @Output() annotationPDFError = new EventEmitter();
  @Output() recommendationAccepted = new EventEmitter();
  @Output() recommendationRejected = new EventEmitter();
  @Output() tableHeaderAdded = new EventEmitter();
  @Output() isPdfLoaded = new EventEmitter();
  @Input() PDFannotations: any;
  @Input() documentId: string;
  @Input() config: PDFconfig;
  loadingMsg: any = "...Loading PDF...";

  private configDiffer: KeyValueDiffer<string, any>;
  /* params declaration start */
  showLoading: boolean = false;
  isFirstLoad: boolean = false;
  renderCircleObj: any = [];
  PDFRenderOption = {
    documentId: this.documentId,
    pdfDocument: null,
    scale: 1,
    rotate: 0
  };
  PDFInfo = {
    totalPages: 1,
    currentPage: 1
  };
  PAGE_HEIGHT: number;
  pageLoaded: any = {};
  pageTmpCanvasLoaded: any = {};
  child_div: any;
  annotationHistory: any = {};
  newannotationuuid: string;
  newannotationObj: any;
  editannotationuuid: string;
  editHistoryLength: string = "0";
  eventName: string;
  img: any;
  newAnnotationMode: string;
  continueAnnotation: boolean = false;
  continueAnnotationId: string;
  addOmrOption: boolean = false;
  addOmrFieldBox: boolean = false;
  omrOptionToContinue: any;
  selectedAnnotation: any;
  copiedSelectedElement: any;
  annotationColors = {
    heading: '#FB5A95',
    paragraph: '#0A00EB',
    key: '#8200EB',
    value: '#EB6500',
    table: '#00A7EB',
    row: '#FF9119',
    column: '#FF9119',
    omr_field: '#4d8d08'
  };
  /* params declaration end */

  constructor(private differs: KeyValueDiffers, private _elementRef: ElementRef, private _renderer: Renderer2) {
    this._renderer.listen(_elementRef.nativeElement, 'mouseleave', (event) => {
      UI.disableEdit();
      this.resetActiveRectClass();
      this.mouseLeaveFrmPDF.emit();
      // this.rectHideandShow("color");
      this.filterAnnotationsbyStatus(this.config.statusFilter);
      document.getElementById('pdfviewer').classList.remove('cropping');
      UI.disableRect(); // to disable the edit annotation
      UI.enableEdit();
      UI.enableRect('area');
      // UI.enableRect('strikeout');
    });

    this.handleAnnotationClick = this.handleAnnotationClick.bind(this);
    this.handleSaveEdit = this.handleSaveEdit.bind(this);
    this.handleDeleteAlert = this.handleDeleteAlert.bind(this);
    this.handleUndo = this.handleUndo.bind(this);
    this.handleAcceptRecommendation = this.handleAcceptRecommendation.bind(this);
    this.handleRejectRecommendation = this.handleRejectRecommendation.bind(this);
    this.handleAddOmrOption = this.handleAddOmrOption.bind(this);
    this.handleAddOmrCheckbox = this.handleAddOmrCheckbox.bind(this);
    this.handleContinueRecommendation = this.handleContinueRecommendation.bind(this);
    this.tableRectMouseOver = this.tableRectMouseOver.bind(this);
    this.tableRectMouseLeave = this.tableRectMouseLeave.bind(this);
    this.allRectMouseOver = this.allRectMouseOver.bind(this);
    this.allRectMouseLeave = this.allRectMouseLeave.bind(this);
    this.tableAddRow = this.tableAddRow.bind(this);
    this.tableAddColumn = this.tableAddColumn.bind(this);
    this.tableRowHeader = this.tableRowHeader.bind(this);
    this.tableColumnHeader = this.tableColumnHeader.bind(this);
    this.omrCheckboxEvent = this.omrCheckboxEvent.bind(this);
  }

  ngOnInit() {
    /*loading script dynamically */
    this.loadScript('./assets/JS/pdf.js');
    this.loadScript('./assets/JS/pdf_viewer.js');
    /* set default paramas */
    this.PDFRenderOption.documentId = this.documentId;
    this.PDFRenderOption.scale = this.config.zoomLevel || 1;
    this.PDFRenderOption.rotate = this.config.rotate || 0;
    this.PDFInfo.currentPage = this.config.currentPage || 1;

    this.config.history_enabled = this.config.history_enabled || true;
    this.config.history_count_per_annotation = this.config.history_count_per_annotation || 5;
    this.config.rendercirclesalongwithindex = this.config.rendercirclesalongwithindex || false;
    this.config.deleteAlert = this.config.deleteAlert || false;
    this.config.useteseract = this.config.useteseract || false;
    /* changes detectio for zoom */
    this.configDiffer = this.differs.find(this.config).create();
    window.addEventListener("scroll", this.pdfScroll.bind(this));
    var that = this;
    /*adapter to save data */
    let MyStoreAdapter = new PDFJSAnnotate.StoreAdapter({
      getAnnotations(documentId, pageNumber) {
        return new Promise((resolve, reject) => {
          var annotations = that.getAnnotations(documentId).filter(i => {
            return i.page === pageNumber && i.class === 'Annotation';
          });
          resolve({
            documentId,
            pageNumber,
            annotations
          });
        });
      },

      getAnnotation(documentId, annotationId) {
        //return Promise.resolve(annotation);

        var annotation;
        for (var i = 0, l = that.PDFannotations.length; i < l; i++) {
          if (that.PDFannotations[i].uuid === annotationId) {
            annotation = that.PDFannotations[i];
            break;
          }
        }

        return Promise.resolve(annotation);

        //return Promise.resolve(that.getAnnotations(documentId)[that.findAnnotation(documentId, annotationId)]);
      },

      addAnnotation(documentId, pageNumber, annotation) {
        let mode = cloneDeep(that.newAnnotationMode);
        var lenOfAnnotations = that.PDFannotations.length;
        return new Promise((resolve, reject) => {
          annotation.class = 'Annotation';
          if (that.newannotationuuid != "" && that.newannotationuuid != undefined) {
            annotation.uuid = that.newannotationuuid;
          } else if (annotation.uuid == "" || annotation.uuid == undefined) {
            annotation.uuid = UUID.UUID();
            if (that.continueAnnotation) {
              let totalLinkedAnnotations = [];
              let elementCount;
              for (var i = 0, l = that.PDFannotations.length; i < l; i++) {
                if (that.PDFannotations[i].element_id === that.continueAnnotationId) {
                  totalLinkedAnnotations.push(that.PDFannotations[i]);
                  elementCount = that.PDFannotations[i].element_count;
                }
              }
              annotation.region_num = totalLinkedAnnotations.length + 1;
              annotation.element_id = that.continueAnnotationId;
              annotation.parent_id = annotation.element_id;
              annotation.element_count = elementCount;
            } else {
              annotation.element_id = 'ele_' + UUID.UUID();
              let totalAnnotationsOnCurrentMode = that.PDFannotations.filter(e => { if (e.field_type === mode) { return e; } });
              const maxValueOfY = Math.max(...totalAnnotationsOnCurrentMode.map(o => o.element_count), 0);
              annotation.element_count = maxValueOfY + 1;
            }
          }
          annotation.page = pageNumber;
          if (mode) {
            if (!(mode === 'table' && (annotation.field_type === 'row' || annotation.field_type === 'column'))) {
              annotation.field_type = mode;
            }
          }
          annotation.accepted = true;
          let annotations = that.getAnnotations(documentId);
          if (annotation.field_type === 'field') {
            let fieldPDFAnnotations = that.PDFannotations.filter(e => { if (e.field_type === 'field') { return e; } });
            let fieldAnnotationsLength = fieldPDFAnnotations.length;
            let annotationContinued = fieldPDFAnnotations.filter(e => { if (e.element_id === that.continueAnnotationId) { return e; } })
            const maxValueOfY = Math.max(...fieldPDFAnnotations.map(o => o.count), 0);
            if (that.continueAnnotation) {
              if (annotationContinued[0].pair_type === 'key') {
                annotation.pair_type = 'key';
                annotation.count = annotationContinued[0].count;
              } else {
                annotation.pair_type = 'value';
                annotation.count = annotationContinued[0].count;
              }
            } else {
              if (fieldAnnotationsLength === 0) {
                annotation.pair_type = 'key';
                annotation.count = maxValueOfY + 1;
                annotation.count = annotation.count.toString();
              } else {
                let lastAnnotationFieldVal = fieldPDFAnnotations.filter(e => { if (e.count === maxValueOfY.toString() && e.pair_type === 'value') { return e; } });
                if (lastAnnotationFieldVal.length !== 0) {
                  annotation.pair_type = 'key';
                  annotation.count = maxValueOfY + 1;
                  annotation.count = annotation.count.toString();
                } else {
                  annotation.pair_type = 'value';
                  annotation.count = maxValueOfY.toString();
                }
              }
            }
          }
          if (annotation.field_type === 'omr_field') {
            let omrFieldPDFAnnotations = that.PDFannotations.filter(e => { if (e.field_type === 'omr_field') { return e; } });
            let omrFieldAnnotationsLength = omrFieldPDFAnnotations.length;
            const maxValueOfY = Math.max(...omrFieldPDFAnnotations.map(o => o.count), 0);
            if (that.addOmrOption) {
              annotation.pair_type = 'omr_value';
              annotation.count = that.omrOptionToContinue.count;
              that.addOmrOption = false;
            } else if (that.addOmrFieldBox) {
              annotation.pair_type = 'omr_fieldbox';
              annotation.count = that.omrOptionToContinue.count;
              annotation.fieldbox_match_id = that.omrOptionToContinue.element_id;
              that.addOmrFieldBox = false;
            } else {
              if (omrFieldAnnotationsLength === 0) {
                annotation.pair_type = 'omr_key';
                annotation.count = maxValueOfY + 1;
                annotation.count = annotation.count.toString();
              } else {
                let lastAnnotationFieldVal = omrFieldPDFAnnotations.filter(e => { if (e.count === maxValueOfY.toString() && e.pair_type === 'omr_value') { return e; } });
                if (lastAnnotationFieldVal.length !== 0) {
                  annotation.pair_type = 'omr_key';
                  annotation.count = maxValueOfY + 1;
                  annotation.count = annotation.count.toString();
                } else {
                  annotation.pair_type = 'omr_value';
                  annotation.count = maxValueOfY.toString();
                }
              }
            }
          }
          annotations.push(annotation);
          document.getElementById('pdfviewer').classList.remove('cropping');
          that.updateAnnotations(documentId, annotations);
          resolve(annotation);

          /*teserract code start */
          if (that.config.useteseract) {
            var canvas = <HTMLCanvasElement>document.getElementById("tmpcanvas" + pageNumber);
            let ctx = canvas.getContext('2d');
            const zoom = 3;
            let imageData = ctx.getImageData(annotation.x * zoom, annotation.y * zoom, annotation.width * zoom, annotation.height * zoom);
            Tesseract.recognize(imageData)
              .then(function (result) {
                that.captureData.emit({ "annotation": annotation, "text": result.text });
                //console.log(result.text)
              });
          }
          /* teseract code end */



          that.annotationAdded.emit(annotation);
          that.rectHideandShow('open');
          that.newannotationuuid = "";
          that.newannotationObj = {};
          if (that.config.rendercirclesalongwithindex) {
            setTimeout(() => {
              that.renderCircles();
            }, 1000);
          }
          // UI.disableRect(); // to disable the edit annotation
        });
      },
      editAnnotation(documentId, annotationId, annotation) {

        return new Promise(function (resolve, reject) {
          var annotations = that.getAnnotations(documentId);
          annotations[
            that.findAnnotation(documentId, annotationId)
          ] = annotation;
          if (annotation.field_type === "row" || annotation.field_type === "column") {
            annotation = that.adjustLineWithInTable(annotation);
          } else{
            that.adjustTableToolOverlay(annotation);
          }
          if (annotation.field_type === "table") {
            that.adjustLinesAfterTableEdit(annotation);
          }
          that.updateAnnotations(documentId, annotations);
          that.annotationEdited.emit(annotation);
          resolve(annotation);
          if (that.config.rendercirclesalongwithindex) {
            setTimeout(() => {
              that.renderCircles();
            }, 1000);
          }

          setTimeout(() => {
            that.editAnnotationEvent(annotation.uuid);
          }, 100);
          // setTimeout(() => {
          //   UI.disableEdit();
          // }, 10000);

          /*teserract code start */
          if (that.config.useteseract) {
            var canvas = <HTMLCanvasElement>document.getElementById("tmpcanvas" + annotation.page);
            let ctx = canvas.getContext('2d');
            const zoom = 3;
            let imageData = ctx.getImageData(annotation.x * zoom, annotation.y * zoom, annotation.width * zoom, annotation.height * zoom);
            Tesseract.recognize(imageData)
              .then(function (result) {
                that.captureData.emit({ "annotation": annotation, "text": result.text });
                //console.log(result.text)
              });
          }
          /* teseract code end */

        });
      },
      deleteAnnotation: function deleteAnnotation(documentId, annotationId) {
        return new Promise(function (resolve, reject) {
          var index = that.findAnnotation(documentId, annotationId);
          that.clearOverlayDiv(annotationId);
          if (index > -1) {
            var annotations = that.getAnnotations(documentId);
            annotations.splice(index, 1);
            that.updateAnnotations(documentId, annotations);
            if (that.config.rendercirclesalongwithindex) {
              that.renderCircles();
            }
            that.annotationDeleted.emit(annotationId);
          }
          resolve(true);
        });
      },

      getComments(documentId, annotationId) {
        return new Promise((resolve, reject) => { });
      },
      addComment(documentId, annotationId, content) { },
      deleteComment(documentId, commentId) { }
    });
    PDFJSAnnotate.setStoreAdapter(MyStoreAdapter);
    this.showLoading = true;
    this.isFirstLoad = true;

    setTimeout(() => {
      this.renderPdf();
    }, 1000);

    /* create dynamic HTML tag for save function when edit annotation happend */

    let child_div = this._renderer.createElement('div');
    child_div.setAttribute('id', 'pdf-annotate-edit-overlay-done');
    child_div.classList.add('crud-annotation');
    /* popover div */
    let popoverdiv = this._renderer.createElement('div');
    popoverdiv.classList.add("popover");
    /* popover-arrow div */
    let popoverarrowdiv = this._renderer.createElement('div');
    popoverarrowdiv.classList.add("popover-arrow");

    let right_icon_created = this._renderer.createElement('i');
    right_icon_created.innerHTML = "<span> Disable Edit </span>";
    <HTMLElement>right_icon_created.classList.add("fa");
    <HTMLElement>right_icon_created.classList.add("fa-check-circle");
    right_icon_created.addEventListener('click', this.handleSaveEdit);

    this._renderer.appendChild(popoverdiv, popoverarrowdiv);
    this._renderer.appendChild(popoverarrowdiv, right_icon_created);

    /*delete confirmation div created */

    let delete_child_div = this._renderer.createElement('div');
    delete_child_div.innerHTML = "<span> Delete </span>";
    delete_child_div.setAttribute('id', 'annotate-delete-overlay-done');
    delete_child_div.classList.add('crud-delete-annotation');
    delete_child_div.classList.add('disableDelete');
    delete_child_div.addEventListener('click', this.handleDeleteAlert.bind(event));
    this._renderer.appendChild(child_div, delete_child_div);

    if (this.config.history_enabled) {
      let undo_icon_created = this._renderer.createElement('i');
      undo_icon_created.innerHTML = "<span id='undo_span_id'> Undo </span>";
      <HTMLElement>undo_icon_created.classList.add("fa");
      <HTMLElement>undo_icon_created.classList.add("fa-undo");
      undo_icon_created.addEventListener('click', this.handleUndo);
      this._renderer.appendChild(popoverarrowdiv, undo_icon_created);
    }
    this._renderer.appendChild(child_div, popoverdiv);
    this.child_div = child_div;
    UI.addEventListener('annotation:click', this.handleAnnotationClick);
    setTimeout(() => {
      UI.disableEdit();
      UI.enableRect("area");
      UI.enableEdit();
    },2000);
  }

  @HostListener('document:keydown.Control.c', ['$event'])
    onKeydownHandler(event: KeyboardEvent) {
      this.copySelectedElement();
    }

  @HostListener('document:keydown.Control.v', ['$event'])
    onKeydownHandler1(event: KeyboardEvent) {
      this.pasteSelectedElement();
    }

  @HostListener('document:keydown.Control.z', ['$event'])
    onKeydownHandler2(event: KeyboardEvent) {
      this.handleUndo();
    }

  @HostListener('document:keydown.Delete', ['$event'])
    onKeydownHandler3(event: KeyboardEvent) {
      this.deleteSelectedItem();
    }

  @HostListener('document:keydown.Backspace', ['$event'])
    onKeydownHandler4(event: KeyboardEvent) {
      this.deleteSelectedItem();
    }

  copySelectedElement() {
    if (this.selectedAnnotation) {
      this.copiedSelectedElement = this.selectedAnnotation;
    }
  }

  pasteSelectedElement() {
    if (this.copiedSelectedElement) {
      let pasteAnnotation = {
        height: this.copiedSelectedElement.height,
        width: this.copiedSelectedElement.width,
        type: 'area',
        x: this.copiedSelectedElement.x + 100,
        y: this.copiedSelectedElement.y + 100
      };
      PDFJSAnnotate.__storeAdapter.addAnnotation(this.documentId, this.copiedSelectedElement.page, pasteAnnotation);
      this.renderPage(this.copiedSelectedElement.page);
    }
  }

  deleteSelectedItem() {
    if (this.selectedAnnotation) {
      let annotationId = this.selectedAnnotation.uuid;
      var annotations = this.getAnnotations(this.documentId);
      var deleteAnnotation = this.selectedAnnotation;
      if (this.continueAnnotation && this.selectedAnnotation.element_id === this.continueAnnotationId) {
        this.continueAnnotation = false;
      }
      let ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-rect-overlay-' + annotationId);
      ele.innerHTML = "";
      let nodes = Array.from(document.querySelectorAll(`[data-pdf-annotate-id="${annotationId}"]`));
      [...nodes].forEach((n) => {
        n.parentNode.removeChild(n);
      });
      this._elementRef.nativeElement.querySelector("#pdf-annotate-edit-overlay").remove();
      PDFJSAnnotate.__storeAdapter.deleteAnnotation(this.documentId, annotationId);
      this.recommendationRejected.emit({"annotationId": annotationId});
      var pairAnnotation;
      if (deleteAnnotation.field_type === 'field' && !deleteAnnotation.parent_id) {
        pairAnnotation = annotations.filter(obj => {if (obj.count===deleteAnnotation.count) {return obj;}});
        var pairAnnotationId;
        if (pairAnnotation.length != 0) {
          for (var i = 0; i < pairAnnotation.length; i++) {
            pairAnnotationId = pairAnnotation[i].uuid;
            let nodes = Array.from(document.querySelectorAll(`[data-pdf-annotate-id="${pairAnnotationId}"]`));
            [...nodes].forEach((n) => {
              n.parentNode.removeChild(n);
            });
            PDFJSAnnotate.__storeAdapter.deleteAnnotation(this.documentId, pairAnnotationId);
          }
          this.recommendationRejected.emit({"annotationId": annotationId});
        }
      } else if(deleteAnnotation.field_type === 'omr_field') {
        if (deleteAnnotation.pair_type === 'omr_key') {
          pairAnnotation = annotations.filter(obj => {if (obj.count===deleteAnnotation.count) {return obj;}});
        }
        if (deleteAnnotation.pair_type === 'omr_value') {
          pairAnnotation = annotations.filter(obj => {if (obj.pair_type === 'omr_fieldbox' && obj.fieldbox_match_id === deleteAnnotation.element_id) {return obj;}});
        }
        var pairAnnotationId;
        if (pairAnnotation.length != 0) {
          for (var i = 0; i < pairAnnotation.length; i++) {
            pairAnnotationId = pairAnnotation[i].uuid;
            let nodes = Array.from(document.querySelectorAll(`[data-pdf-annotate-id="${pairAnnotationId}"]`));
            [...nodes].forEach((n) => {
              n.parentNode.removeChild(n);
            });
            PDFJSAnnotate.__storeAdapter.deleteAnnotation(this.documentId, pairAnnotationId);
          }
          this.recommendationRejected.emit({"annotationId": annotationId});
        }
      } else if (deleteAnnotation.field_type === 'table') {
        var rowsAndColumns = annotations.filter(obj => {if (obj.parentUUID===deleteAnnotation.uuid) {return obj;}});
        if (rowsAndColumns.length != 0) {
          rowsAndColumns.forEach(element => {
            var linkedAnnotationId = element.uuid;
            let nodes = Array.from(document.querySelectorAll(`[data-pdf-annotate-id="${linkedAnnotationId}"]`));
            [...nodes].forEach((n) => {
              n.parentNode.removeChild(n);
            });
            PDFJSAnnotate.__storeAdapter.deleteAnnotation(this.documentId, linkedAnnotationId);
            this.recommendationRejected.emit({"annotationId": linkedAnnotationId});
          });
        }
      } else if (!deleteAnnotation.parent_id && !(deleteAnnotation.field_type === 'row' || deleteAnnotation.field_type === 'column')) {
        var linkedAnnotations = annotations.filter(obj => { if (obj.parent_id === deleteAnnotation.element_id) { return obj } });
        var linkedAnnotationId;
        for (var i = 0; i < linkedAnnotations.length; i++) {
          linkedAnnotationId = linkedAnnotations[i].uuid;
          let nodes = Array.from(document.querySelectorAll(`[data-pdf-annotate-id="${linkedAnnotationId}"]`));
          [...nodes].forEach((n) => {
            n.parentNode.removeChild(n);
          });
          PDFJSAnnotate.__storeAdapter.deleteAnnotation(this.documentId, linkedAnnotationId);
        }
        this.recommendationRejected.emit({"annotationId": linkedAnnotationId});
      }
      if (!(deleteAnnotation.field_type === 'row' || deleteAnnotation.field_type === 'column' || deleteAnnotation.field_type === 'table')) {
        this.renderPage(this.selectedAnnotation.page);
      }
    }
  }

  handleSaveEdit = function () {
    UI.disableEdit();
    this.resetActiveRectClass();
    this.mouseLeaveFrmPDF.emit();
  }

  setAnnotationMode(mode) {
    if (this.newAnnotationMode !== mode.field_type) {
      this.continueAnnotation = false;
    }
    this.newAnnotationMode = mode.field_type;
    UI.disableEdit();
    UI.enableRect('area');
    UI.enableEdit();
  }

  adjustTableToolOverlay(annotation) {
    var div = document.getElementById("pdf-annotate-rect-overlay-"+annotation.uuid);
    var tableDiv = document.getElementById("pdf-annotate-table-tools-overlay-"+annotation.uuid);
    var svgs = document.querySelectorAll(
      "svg[data-pdf-annotate-page='" + annotation.page + "']"
    );
    var viewPortDetails = svgs[0].getAttribute("data-pdf-annotate-viewport");
    var viewPortDetailsParsed = JSON.parse(viewPortDetails);
    var svgWidth = svgs[0].getAttribute("width");
    var top = (viewPortDetailsParsed.scale*annotation.y)-3;
    var right = parseInt(svgWidth) - (annotation.x + annotation.width)*viewPortDetailsParsed.scale;
    div.style.top = (top-19) + 'px';
    div.style.right = right + 'px';
    tableDiv.style.top = (top-25) + 'px';
    tableDiv.style.right = right + 'px';
  }

  adjustOverlayWithIntable(annotation) {
    var div = document.getElementById("pdf-annotate-edit-overlay");
    var svgs = document.querySelectorAll(
      "svg[data-pdf-annotate-page='" + annotation.page + "']"
    );
    var viewPortDetails = svgs[0].getAttribute("data-pdf-annotate-viewport");
    var viewPortDetailsParsed = JSON.parse(viewPortDetails);
    var svgWidth = svgs[0].getAttribute("width");
    var top = (viewPortDetailsParsed.scale*annotation.rectangles[0].y) - 3;
    var left = (annotation.rectangles[0].x)*viewPortDetailsParsed.scale - 3;
    div.style.top = top + 'px';
    div.style.left = left + 'px';
  }

  modifySvgLine(annotation, type) {
    var g = document.querySelector(
      "g[data-pdf-annotate-id='" + annotation.uuid + "']"
    );
    if (type === 'row') {
      g.setAttribute("x", annotation.rectangles[0].x);
      g.children[0].setAttribute("x1", annotation.rectangles[0].x);
      g.children[0].setAttribute("x2", annotation.rectangles[0].x+annotation.rectangles[0].width);
      g.children[0].setAttribute("y1", annotation.rectangles[0].y);
      g.children[0].setAttribute("y2", annotation.rectangles[0].y);
    } else {
      g.setAttribute("y", annotation.rectangles[0].y);
      g.children[0].setAttribute("x1", annotation.rectangles[0].x);
      g.children[0].setAttribute("x2", annotation.rectangles[0].x);
      g.children[0].setAttribute("y1", annotation.rectangles[0].y);
      g.children[0].setAttribute("y2", annotation.rectangles[0].y+annotation.rectangles[0].height);
    }
  }

  adjustLineWithInTable(annotation) {
    var tableAnnotation;
    for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
      if (this.PDFannotations[i].uuid === annotation.parentUUID) {
        tableAnnotation = this.PDFannotations[i];
        break;
      }
    }
    switch(annotation.field_type) {
      case 'row':
        annotation.rectangles[0].x = tableAnnotation.x;
        if (!(tableAnnotation.y < annotation.rectangles[0].y)) {
          annotation.rectangles[0].y = tableAnnotation.y + 10;
        } else if (!(annotation.rectangles[0].y < tableAnnotation.y+tableAnnotation.height)) {
          annotation.rectangles[0].y = tableAnnotation.y + tableAnnotation.height - 50;
        }
        this.modifySvgLine(annotation, 'row');
        this.adjustOverlayWithIntable(annotation);
        break;
      case 'column':
        annotation.rectangles[0].y = tableAnnotation.y;
        if (!(tableAnnotation.x < annotation.rectangles[0].x)) {
          annotation.rectangles[0].x = tableAnnotation.x + 10;
        } else if (!(annotation.rectangles[0].x < tableAnnotation.x+tableAnnotation.width)) {
          annotation.rectangles[0].x = tableAnnotation.x + tableAnnotation.width - 50;
        }
        this.modifySvgLine(annotation, 'column');
        this.adjustOverlayWithIntable(annotation);
        break;
    }
    return annotation;
  }

  adjustLinesAfterTableEdit(annotation) {
    var tableAnnotation = annotation;
    var rowsAndColumns = this.PDFannotations.filter(obj => {if (obj.parentUUID===annotation.uuid) {return obj;}});
    rowsAndColumns.forEach(element => {
      var g = document.querySelector(
        "g[data-pdf-annotate-id='" + element.uuid + "']"
      );
      if (element.field_type === 'row') {
        element.rectangles[0].x = tableAnnotation.x;
        element.rectangles[0].y = tableAnnotation.y - (this.selectedAnnotation.y - element.rectangles[0].y);
        element.rectangles[0].width = tableAnnotation.width;
        g.setAttribute("x", element.rectangles[0].x);
        g.children[0].setAttribute("x1", element.rectangles[0].x);
        g.children[0].setAttribute("x2", element.rectangles[0].x+element.rectangles[0].width);
        g.children[0].setAttribute("y1", element.rectangles[0].y);
        g.children[0].setAttribute("y2", element.rectangles[0].y);
      }
      if (element.field_type === 'column') {
        element.rectangles[0].x = tableAnnotation.x + (element.rectangles[0].x - this.selectedAnnotation.x);
        element.rectangles[0].y = tableAnnotation.y;
        element.rectangles[0].height = tableAnnotation.height;
        g.setAttribute("y", element.rectangles[0].y);
        g.children[0].setAttribute("x1", element.rectangles[0].x);
        g.children[0].setAttribute("x2", element.rectangles[0].x);
        g.children[0].setAttribute("y1", element.rectangles[0].y);
        g.children[0].setAttribute("y2", element.rectangles[0].y+element.rectangles[0].height);
      }
    });
  }

  handleAddOmrOption(e) {
    e.stopPropagation();
    let annotationId = e.currentTarget.id;
    this.addOmrOption = true;
    this.addOmrFieldBox = false;
    let annotation;
    for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
      if (this.PDFannotations[i].uuid === e.currentTarget.id) {
        annotation = this.PDFannotations[i];
        break;
      }
    }
    this.omrOptionToContinue = annotation;
  }

  handleAddOmrCheckbox(e) {
    e.stopPropagation();
    let annotationId = e.currentTarget.id;
    this.addOmrOption = false;
    this.addOmrFieldBox = true;
    let annotation;
    for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
      if (this.PDFannotations[i].uuid === e.currentTarget.id) {
        annotation = this.PDFannotations[i];
        break;
      }
    }
    this.omrOptionToContinue = annotation;
  }

  handleAcceptRecommendation = function (e) {
    e.stopPropagation();
    let ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-rect-overlay-' + e.currentTarget.id);
    let acceptIcon = ele.querySelector(".checkIconStyle");
    acceptIcon.classList.remove("checkIconStyle");
    acceptIcon.classList.add("acceptedCheckStyle");
    // ele.innerHTML = "";
    // let accepted = this._renderer.createElement("span");
    // accepted.innerHTML = "<i class='fa fa-check-circle acceptCheckStyle'></i>"
    // ele.appendChild(accepted);
    for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
      if (this.PDFannotations[i].uuid === e.currentTarget.id) {
        this.PDFannotations[i].accepted = true;
        break;
      }
    }
    this.recommendationAccepted.emit({"annotationId": e.currentTarget.id});
    let annotationIndex = this.findAnnotation(this.documentId, e.currentTarget.id);
  }

  handleRejectRecommendation = function (e) {
    e.stopPropagation();
    let annotationId = e.currentTarget.id;
    var index = this.findAnnotation(this.documentId, annotationId);
    var annotations = this.getAnnotations(this.documentId);
    var deleteAnnotation = annotations[index];
    if (this.continueAnnotation && deleteAnnotation.element_id === this.continueAnnotationId) {
      this.continueAnnotation = false;
    }
    let ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-rect-overlay-' + annotationId);
    ele.innerHTML = "";
    let nodes = Array.from(document.querySelectorAll(`[data-pdf-annotate-id="${annotationId}"]`));
    [...nodes].forEach((n) => {
      n.parentNode.removeChild(n);
    });
    PDFJSAnnotate.__storeAdapter.deleteAnnotation(this.documentId, annotationId);
    this.recommendationRejected.emit({"annotationId": annotationId});
    var pairAnnotation;
    if (deleteAnnotation.field_type === 'field' && !deleteAnnotation.parent_id) {
      pairAnnotation = annotations.filter(obj => {if (obj.count===deleteAnnotation.count) {return obj;}});
      var pairAnnotationId;
      if (pairAnnotation.length != 0) {
        for (var i = 0; i < pairAnnotation.length; i++) {
          pairAnnotationId = pairAnnotation[i].uuid;
          let nodes = Array.from(document.querySelectorAll(`[data-pdf-annotate-id="${pairAnnotationId}"]`));
          [...nodes].forEach((n) => {
            n.parentNode.removeChild(n);
          });
          PDFJSAnnotate.__storeAdapter.deleteAnnotation(this.documentId, pairAnnotationId);
        }
        this.recommendationRejected.emit({"annotationId": annotationId});
      }
    } else if(deleteAnnotation.field_type === 'omr_field') {
      if (deleteAnnotation.pair_type === 'omr_key') {
        pairAnnotation = annotations.filter(obj => {if (obj.count===deleteAnnotation.count) {return obj;}});
      }
      if (deleteAnnotation.pair_type === 'omr_value') {
        pairAnnotation = annotations.filter(obj => {if (obj.pair_type === 'omr_fieldbox' && obj.fieldbox_match_id === deleteAnnotation.element_id) {return obj;}});
      }
      var pairAnnotationId;
      if (pairAnnotation.length != 0) {
        for (var i = 0; i < pairAnnotation.length; i++) {
          pairAnnotationId = pairAnnotation[i].uuid;
          let nodes = Array.from(document.querySelectorAll(`[data-pdf-annotate-id="${pairAnnotationId}"]`));
          [...nodes].forEach((n) => {
            n.parentNode.removeChild(n);
          });
          PDFJSAnnotate.__storeAdapter.deleteAnnotation(this.documentId, pairAnnotationId);
        }
        this.recommendationRejected.emit({"annotationId": annotationId});
      }
    } else if (deleteAnnotation.field_type === 'table') {
      var rowsAndColumns = annotations.filter(obj => {if (obj.parentUUID===deleteAnnotation.uuid) {return obj;}});
      if (rowsAndColumns.length != 0) {
        rowsAndColumns.forEach(element => {
          var linkedAnnotationId = element.uuid;
          let nodes = Array.from(document.querySelectorAll(`[data-pdf-annotate-id="${linkedAnnotationId}"]`));
          [...nodes].forEach((n) => {
            n.parentNode.removeChild(n);
          });
          PDFJSAnnotate.__storeAdapter.deleteAnnotation(this.documentId, linkedAnnotationId);
          this.recommendationRejected.emit({"annotationId": linkedAnnotationId});
        });
      }
    } else if (!deleteAnnotation.parent_id) {
      var linkedAnnotations = annotations.filter(obj => { if (obj.parent_id === deleteAnnotation.element_id) { return obj } });
      var linkedAnnotationId;
      for (var i = 0; i < linkedAnnotations.length; i++) {
        linkedAnnotationId = linkedAnnotations[i].uuid;
        let nodes = Array.from(document.querySelectorAll(`[data-pdf-annotate-id="${linkedAnnotationId}"]`));
        [...nodes].forEach((n) => {
          n.parentNode.removeChild(n);
        });
        PDFJSAnnotate.__storeAdapter.deleteAnnotation(this.documentId, linkedAnnotationId);
      }
      this.recommendationRejected.emit({"annotationId": linkedAnnotationId});
    }
  }

  handleContinueRecommendation = function (e) {
    e.stopPropagation();
    let annotationId = e.currentTarget.id;
    this.continueAnnotation = !this.continueAnnotation;
    let annotation;
    for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
      if (this.PDFannotations[i].uuid === e.currentTarget.id) {
        annotation = this.PDFannotations[i];
        break;
      }
    }
    if (this.continueAnnotation) {
      this.continueAnnotationId = annotation.element_id;
      this.setAnnotationMode({uuid: '', field_type: annotation.field_type});
    }
    let ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-rect-overlay-' + annotationId);
    if (this.continueAnnotation) {
      let continueIcon = ele.querySelector(".continueIconDeactive");
      continueIcon.setAttribute('title', 'deactivate continue');
      continueIcon.classList.remove("continueIconDeactive");
      continueIcon.classList.add("continueIconActive");
    } else {
      let continueIcon = ele.querySelector(".continueIconActive");
      continueIcon.setAttribute('title', 'activate continue');
      continueIcon.classList.remove("continueIconActive");
      continueIcon.classList.add("continueIconDeactive");
    }
  }

  clearOverlayDiv(annotationId) {
    var deletedAnnotation;
    for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
      if (this.PDFannotations[i].uuid === annotationId) {
        deletedAnnotation = this.PDFannotations[i];
        break;
      }
    }
    let ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-rect-overlay-' + annotationId);
    if (ele) {
      ele.innerHTML = '';
    }
    if (deletedAnnotation.field_type === 'table') {
      let ele1 = this._elementRef.nativeElement.querySelector('#pdf-annotate-table-tools-overlay-' + annotationId);
      if (ele1) {
        ele1.innerHTML = '';
      }
    }
  }

  handleDeleteAlert = function (e) {
    if (this.config.deleteAlert) {
      var annotationId = document.getElementById("pdf-annotate-edit-overlay").getAttribute("data-target-id");
      let nodes = Array.from(document.querySelectorAll(`[data-pdf-annotate-id="${annotationId}"]`));
      this.clearOverlayDiv(annotationId);
      [...nodes].forEach((n) => {
        n.parentNode.removeChild(n);
      });
      PDFJSAnnotate.__storeAdapter.deleteAnnotation(this.documentId, annotationId);
    }
  }

  /* load pdf's on scroll  */
  @HostListener('window:scroll', ['$event'])
  pdfScroll(event) {
    let visiblePageNum = Math.round(event.target.scrollTop / this.PAGE_HEIGHT) + 1;
    if (visiblePageNum <= this.PDFInfo.totalPages) {
      this.pageChanged.emit(visiblePageNum.toString());
      this.PDFInfo.currentPage = visiblePageNum;
      if (this.config.loadallpages === false) {
        var visiblePage = document.querySelector('.page[data-page-number="' + visiblePageNum + '"][data-loaded="false"]');
        if (visiblePage && this.pageLoaded[visiblePageNum] == undefined) {
          this.pageLoaded[visiblePageNum] = true;
          let that = this;
          setTimeout(function () {
            this.renderPage(visiblePageNum);
            if (this.newannotationuuid) {
              this.rectHideandShow('transparent');
            }
            this.filterAnnotationsbyStatus(this.config.statusFilter);
          }.bind(this), 1000);
        }
        else {
          this.pageLoaded[visiblePageNum] = true;
          this.filterAnnotationsbyStatus(this.config.statusFilter);
        }
      }
      if (!this.renderCircleObj[this.PDFInfo.currentPage]) {
        if (this.config.rendercirclesalongwithindex) {

          setTimeout(() => {
            this.renderCircles();
          }, 1000);
        }
      }
    }
    else {
      this.PDFInfo.currentPage = this.PDFInfo.totalPages;
      this.filterAnnotationsbyStatus(this.config.statusFilter);
    }
    // setTimeout(function() {
    // this.filterAnnotationsbyStatus(this.config.statusFilter);
    // }.bind(this), 1000);
  }

  /* function is used to load pdf */
  renderPdf() {
    PDFJS.workerSrc = 'assets/JS/pdf.worker.js';
    PDFJS.getDocument(this.PDFRenderOption.documentId).then(
      pdf => {
        this.PDFRenderOption.pdfDocument = pdf;
        this.PDFInfo.totalPages = this.PDFRenderOption.pdfDocument.pdfInfo.numPages;
        this.config.totalpages = this.PDFInfo.totalPages;
        let viewer = document.getElementById('viewer');
        if (viewer == null)
          return;
        viewer.innerHTML = '';
        for (let i = 0; i < this.PDFInfo.totalPages; i++) {
          let page = UI.createPage(i + 1);
          viewer.appendChild(page);
        }
        if (this.PDFInfo.currentPage === undefined)
          this.PDFInfo.currentPage = 1;
        this.renderView(false, this.PDFInfo.currentPage);
        // UI.disableEdit();
        UI.enableEdit();
        UI.enableRect('area');
        UI.enableRect('strikeout');
      },
      error => {
        console.log('Error', error);
        var loadinghtml = document.getElementById('loadingError');
        if (loadinghtml)
          document.getElementById('loadingError').innerHTML = "Document(s) for case id  <b>" + this.documentId + " </b> is not found. To resolve this issue, contact your system administrator.";
        this.annotationPDFError.emit("error");
      }
    );
  }

  /* function is used to render the page in UI */
  renderView(isZoomchanged, pageNumber) {
    UI.renderPage(pageNumber, this.PDFRenderOption).then(([pdfPage, annotations]) => {
      if (this.isFirstLoad || isZoomchanged) {
        let canvasWidth = document.getElementById('pdfviewer');
        if (this.isFirstLoad) {
          this.PDFRenderOption.scale = parseFloat(
            (canvasWidth.offsetWidth / pdfPage.view[2]).toFixed(2)
          );
          this.config.zoomLevel = this.PDFRenderOption.scale;
          this.isFirstLoad = false;
          return;
        }
        this.renderPdf();
        return;
      }
      let viewport = pdfPage.getViewport(
        this.PDFRenderOption.scale,
        this.PDFRenderOption.rotate
      );
      this.PAGE_HEIGHT = viewport.height;
    });

    if (this.config.rendercirclesalongwithindex) { // load circle based on config
      setTimeout(() => {
        this.renderCircles();
      }, 2000);
    }

    if (this.config.loadallpages === true) { // load all pages at time
      setTimeout(() => {
        for (var i = 2; i <= this.PDFInfo.totalPages; i++) {
          UI.renderPage(i, this.PDFRenderOption).then(([pdfPage, annotations]) => {
            let viewport = pdfPage.getViewport(
              this.PDFRenderOption.scale,
              this.PDFRenderOption.rotate
            );
            this.PAGE_HEIGHT = viewport.height;
          });
        }

        if (this.config.rendercirclesalongwithindex) { // load circle based on config
          setTimeout(() => {
            this.renderCircles();
          }, 1000);
        }

      }, 2000);
    }
    this.showLoading = false;
    this.isPdfLoaded.emit(true);
  }

  /* function is used to create the circle on rectangle annotaion along with index as text in UI */
  renderCircles() {
    var svgs = document.querySelectorAll(
      "svg[data-pdf-annotate-page='" + this.PDFInfo.currentPage + "']"
    );
    for (var i = 0; i < svgs.length; i++) {
      var allrects = svgs[i].querySelectorAll('rect');
      var tmpRects: any = [];
      // for (var j = 0; j < allrects.length; j++) {
      //   tmpRects.push(allrects[j]);
      // }
      tmpRects = allrects;
      var sortedRects = [].slice.call(tmpRects).sort(function (rect1, rect2) {
        return rect1.getAttribute('y') - rect2.getAttribute('y');
      });

      // const rectArray = Array.from(svgs[i].querySelectorAll('rect'));
      // rectArray.forEach(function (rect) {
      //   rect.remove();
      // });
      // const circleArray = Array.from(svgs[i].querySelectorAll('circle'));
      // circleArray.forEach(function (circle) {
      //   circle.remove();
      // });
      // const textArray = Array.from(svgs[i].querySelectorAll('text'));
      // textArray.forEach(function (text) {
      //   text.remove();
      // });

      svgs[i].innerHTML = "";

      for (var k = sortedRects.length - 1; k >= 0; k--) {
        let circle = this.createCircle(sortedRects[k]);
        let text = this.createText(circle);
        svgs[i].appendChild(sortedRects[k]);
        svgs[i].appendChild(circle);
        svgs[i].appendChild(text);
      }
    }

    this.renderCircleObj[this.PDFInfo.currentPage] = true;
  }

  /* function is used to create svg circtle and append to svg */
  createCircle(rect) {
    var circle = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'circle'
    );
    var circlex = parseInt(rect.getAttribute('x')) * this.PDFRenderOption.scale;
    var circley =
      (parseInt(rect.getAttribute('y')) +
        Math.round(rect.getAttribute('height') / 2)) *
      this.PDFRenderOption.scale;
    var cradius = parseFloat((12 * this.PDFRenderOption.scale + 3).toFixed(2));
    circle.setAttributeNS(null, 'cx', circlex.toString());
    circle.setAttributeNS(null, 'cy', circley.toString());
    circle.setAttributeNS(
      null,
      'data-circle-pdf-annotate-id',
      rect.getAttribute('data-pdf-annotate-id')
    );
    circle.setAttributeNS(null, 'r', cradius.toString());
    circle.setAttributeNS(
      null,
      'style',
      'fill:' +
      rect.getAttribute('stroke') +
      '; stroke: green; stroke-width: 1px;'
    );
    return circle;
  }

  /* function is used to create svg text and append to svg */
  createText(circle) {
    var text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    var textx =
      Math.round(circle.getAttribute('cx')) -
      Math.round(circle.getAttribute('r') / 2);
    var texty =
      Math.round(circle.getAttribute('cy')) +
      Math.round(circle.getAttribute('r') / 2);
    text.setAttributeNS(null, 'x', textx.toString());
    text.setAttributeNS(null, 'y', texty.toString());
    text.setAttributeNS(
      null,
      'data-text-pdf-annotate-id',
      circle.getAttribute('data-circle-pdf-annotate-id')
    );
    text.textContent = this.getAnnotationIndex(
      circle.getAttribute('data-circle-pdf-annotate-id')
    );
    return text;
  }

  /* function is used to get index of annotation from right side data object */
  getAnnotationIndex(tmpId) {
    let anno_index = this.findAnnotation(this.documentId, tmpId);
    return anno_index.toString();
  }

  /* function is used to load initial annotations */
  getAnnotations(documentId) {
    this.setRecommendationIcons();
    this.setRectMouseoverEvent();
    return this.PDFannotations || [];
  }

  setRecommendationIcons() {
    setTimeout(() => {
      this.PDFannotations.forEach((annotation: any) => {
        if (annotation.field_type !== 'row' && annotation.field_type !== 'column') {
          let recommended_icons_div = this._renderer.createElement('div');
          recommended_icons_div.setAttribute("style", "position: absolute;display: flex;flex-direction: row;right: 0;z-index: 1000");
          recommended_icons_div.setAttribute("id", "tools-div-" + annotation.uuid);
          recommended_icons_div.setAttribute("data-target-id", annotation.uuid);
          recommended_icons_div.classList.add('recommendation-div');
          let accept_icon = this._renderer.createElement('span');
          accept_icon.setAttribute("id", annotation.uuid);
          accept_icon.setAttribute("style", "margin-right: 3px");
          accept_icon.setAttribute("data-target-id", annotation.uuid);
          if (!annotation.accepted) {
            accept_icon.innerHTML = "<i class='fa fa-check checkIconStyle'></i>";
          }
          let continueIcon = this._renderer.createElement('div');
          continueIcon.classList.add('continueIconDiv');
          continueIcon.setAttribute("data-target-id", annotation.uuid);
          if (!annotation.parent_id) {
            continueIcon.setAttribute("id", annotation.uuid);
            continueIcon.setAttribute("style", "margin-right: 3px");
            if (this.continueAnnotation && this.continueAnnotationId === annotation.element_id) {
              continueIcon.setAttribute('title', 'deactivate continue');
              continueIcon.classList.remove('continueIconDeactive');
              continueIcon.classList.add('continueIconActive');
              // continueIcon.innerHTML = "<i class='fa fa-arrow-right continueIconActive'></i>";
            } else {
              continueIcon.setAttribute('title', 'activate continue');
              continueIcon.classList.remove('continueIconActive');
              continueIcon.classList.add('continueIconDeactive');
              // continueIcon.innerHTML = "<i class='fa fa-arrow-right continueIconDeactive'></i>";
            }
          }
          let reject_icon = this._renderer.createElement('div');
          reject_icon.setAttribute('title', 'delete');
          reject_icon.setAttribute("id", annotation.uuid);
          reject_icon.setAttribute("data-target-id", annotation.uuid);
          reject_icon.classList.add('rejectIconDiv');
          let add_omr_option_icon = this._renderer.createElement('div');
          add_omr_option_icon.setAttribute('title', 'add option');
          add_omr_option_icon.setAttribute("id", annotation.uuid);
          add_omr_option_icon.setAttribute("data-target-id", annotation.uuid);
          add_omr_option_icon.classList.add('omrOptionIconDiv');
          let add_omr_checkbox_icon = this._renderer.createElement('div');
          add_omr_checkbox_icon.setAttribute('title', 'add field box');
          add_omr_checkbox_icon.setAttribute("id", annotation.uuid);
          add_omr_checkbox_icon.setAttribute("data-target-id", annotation.uuid);
          add_omr_checkbox_icon.classList.add('omrFieldBoxIconDiv');
          // omr checkbox
          var omrCheckboxLabel = this._renderer.createElement('label');
          omrCheckboxLabel.setAttribute("title", 'isChecked');
          omrCheckboxLabel.setAttribute("id", annotation.uuid);
          omrCheckboxLabel.setAttribute('data-target-id', annotation.uuid);
          omrCheckboxLabel.classList.add('omrCheckboxLabel');
          omrCheckboxLabel.classList.add('tab-header');
          omrCheckboxLabel.addEventListener("click", this.omrCheckboxEvent.bind(event));
          if (annotation.isChecked) {
            var id = annotation.uuid;
            omrCheckboxLabel.innerHTML = '<input type="checkbox" checked="checked" id="'+id+'"><span class="checkmark" data-target-id="'+ annotation.uuid +'" id="'+id+'"></span>';
          } else {
            var id = annotation.uuid;
            omrCheckboxLabel.innerHTML = '<input type="checkbox" id="'+id+'"><span class="checkmark" data-target-id="'+ annotation.uuid +'" id="'+id+'"></span>';
          }
          // reject_icon.innerHTML = "<i class='fa fa-times timesIconStyle'></i>";
          this._renderer.appendChild(recommended_icons_div, accept_icon);
          if (!annotation.parent_id && !(annotation.field_type === 'omr_field')) {
            this._renderer.appendChild(recommended_icons_div, continueIcon);
          }
          if (annotation.field_type === 'omr_field' && annotation.pair_type === 'omr_value') {
            this._renderer.appendChild(recommended_icons_div, add_omr_option_icon);
            this._renderer.appendChild(recommended_icons_div, add_omr_checkbox_icon);
          }
          if (annotation.field_type === 'omr_field' && annotation.pair_type === 'omr_fieldbox') {
            this._renderer.appendChild(recommended_icons_div, omrCheckboxLabel);
          }
          this._renderer.appendChild(recommended_icons_div, reject_icon);
          let annotation_label = this._renderer.createElement('div');
          annotation_label.classList.add('annotationLabelRep');
          var annotationWidth;
          var svgs = document.querySelectorAll("svg[data-pdf-annotate-page='" + annotation.page + "']");
          if (svgs.length > 0 && annotation.field_type !== 'field') {
            var viewPortDetails = svgs[0].getAttribute("data-pdf-annotate-viewport");
            var viewPortDetailsParsed = JSON.parse(viewPortDetails);
            var labelPosition = viewPortDetailsParsed.scale * annotation.width;
            annotationWidth = viewPortDetailsParsed.scale * annotation.width;
            annotation_label.setAttribute("style", "position: absolute;background: " + this.annotationColors[annotation.field_type] + ";color: white;font-weight: 600;font-size: 12px;padding: 0px 5px 0px 5px;top: 4px;max-width: " + annotationWidth + "px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;left: -" + labelPosition + "px;" );
            if (this.config.showLabels) {
              annotation_label.classList.remove("displayNone");
              annotation_label.classList.add("displayBlock");
            } else {
              annotation_label.classList.remove("displayBlock");
              annotation_label.classList.add("displayNone");
            }
          }
          if (!annotation.parent_id && annotation.field_type !== 'field') {
            var char = annotation.field_type.charAt(0);
            char = char.toUpperCase();
            annotation_label.innerText = char + annotation.element_count;
            annotation_label.addEventListener('mouseover', (event) => {
              event.target.style.maxWidth = 'none';
              event.target.style.overflow = 'visible';
            });
            annotation_label.addEventListener('mouseleave', (event) => {
              event.target.style.maxWidth = annotationWidth + 'px';
              event.target.style.overflow = 'hidden';
            });
          }
          if (annotation.parent_id && annotation.field_type !== 'field') {
            var char = annotation.field_type.charAt(0);
            char = char.toUpperCase();
            annotation_label.innerText = "Continuation of " + char + annotation.element_count;
            annotation_label.addEventListener('mouseover', (event) => {
              event.target.style.maxWidth = 'none';
              event.target.style.overflow = 'visible';
            });
            annotation_label.addEventListener('mouseleave', (event) => {
              event.target.style.maxWidth = annotationWidth + 'px';
              event.target.style.overflow = 'hidden';
            });
          }
          if (annotation.field_type === 'omr_field') {
            if (annotation.pair_type === 'omr_key') {
              annotation_label.innerText = 'OMR ' + annotation.count + ' - K';
            }
            if (annotation.pair_type === 'omr_value') {
              annotation_label.innerText = 'OMR ' + annotation.count + ' - V';
            }
            if (annotation.pair_type === 'omr_fieldbox') {
              annotation_label.innerText = '';
            }
            annotation_label.addEventListener('mouseover', (event) => {
              event.target.style.maxWidth = 'none';
              event.target.style.overflow = 'visible';
            });
            annotation_label.addEventListener('mouseleave', (event) => {
              event.target.style.maxWidth = annotationWidth + 'px';
              event.target.style.overflow = 'hidden';
            });
          }
          accept_icon.addEventListener('click', this.handleAcceptRecommendation.bind(event));
          reject_icon.addEventListener('click', this.handleRejectRecommendation.bind(event));
          add_omr_option_icon.addEventListener('click', this.handleAddOmrOption.bind(event));
          add_omr_checkbox_icon.addEventListener('click', this.handleAddOmrCheckbox.bind(event));
          if (!annotation.parent_id) {
            continueIcon.addEventListener('click', this.handleContinueRecommendation.bind(event));
          }
          let ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-rect-overlay-' + annotation.uuid);
          if (ele) {
            ele.innerHTML = '';
            recommended_icons_div.style.display = 'none';
            ele.appendChild(recommended_icons_div);
          }
          if (ele && annotation.field_type === 'table') {
            ele.innerHTML = '';
            this.tableToolsDivConstruct(annotation.uuid);
          }
          if (ele && annotation.field_type !== 'field') {
            ele.appendChild(annotation_label);
          }
          if (annotation.field_type === 'field') {
            let keyvalue_rep_div = this._renderer.createElement('div');
            keyvalue_rep_div.classList.add("keyValueLabelRep");
            if (this.config.showLabels) {
              keyvalue_rep_div.classList.remove("displayNone");
              keyvalue_rep_div.classList.add("displayBlock");
            } else {
              keyvalue_rep_div.classList.remove("displayBlock");
              keyvalue_rep_div.classList.add("displayNone");
            }
            let k_text = this._renderer.createElement('span');
            k_text.setAttribute("id", annotation.uuid);
            if (annotation.pair_type === 'key') {
              var svgs = document.querySelectorAll("svg[data-pdf-annotate-page='" + annotation.page + "']");
              if (svgs.length > 0) {
                var viewPortDetails = svgs[0].getAttribute("data-pdf-annotate-viewport");
                var viewPortDetailsParsed = JSON.parse(viewPortDetails);
                var keyLeftPosition = viewPortDetailsParsed.scale * annotation.width;
                keyvalue_rep_div.setAttribute("style", "position: absolute;background: #8200EB;color: white;font-weight: 600;font-size: 12px;padding: 0px 5px 0px 5px;top: 21px;right: " + keyLeftPosition + "px;" );
              }
              k_text.innerText = annotation.count;
            } else {
              keyvalue_rep_div.setAttribute("style", "position: absolute;background: #EB6500;color: white;font-weight: 600;font-size: 12px;padding: 0px 5px 0px 5px;top: 21px;");
              k_text.innerText = annotation.count;
            }
            this._renderer.appendChild(keyvalue_rep_div, k_text);
            let ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-rect-overlay-' + annotation.uuid);
            if (ele) {
              ele.appendChild(keyvalue_rep_div);
            }
          }
        }
      });
    }, 500);
  }

  tableToolsDivConstruct(annotateId) {
    var tableAnnotation;
    for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
      if (this.PDFannotations[i].uuid === annotateId) {
        tableAnnotation = this.PDFannotations[i];
        break;
      }
    }
    let table_tools_div = this._renderer.createElement("div");
    table_tools_div.classList.add('table-tools-cls');
    table_tools_div.setAttribute("id", "table-tools-div-" + annotateId);
    table_tools_div.style.zIndex = 1000;
    table_tools_div.setAttribute("data-target-id", annotateId);
    /* add column icon */
    let addColumnIcon = this._renderer.createElement("div");
    addColumnIcon.setAttribute('title', 'Add column');
    addColumnIcon.classList.add('addColumn');
    addColumnIcon.setAttribute("id", annotateId);
    addColumnIcon.setAttribute("data-target-id", annotateId);
    addColumnIcon.style.marginRight = "10px";
    /* add row icon */
    let addRowIcon = this._renderer.createElement("div");
    addRowIcon.setAttribute('title', 'Add row');
    addRowIcon.classList.add('addRow');
    addRowIcon.setAttribute("id", annotateId);
    addRowIcon.setAttribute("data-target-id", annotateId);
    addRowIcon.style.marginRight = "10px";
    /* add header icon */
    var addHeaderIcon = this._renderer.createElement("div");
    addHeaderIcon.setAttribute('title', 'Add header');
    addHeaderIcon.setAttribute('data-toggle', 'dropdown');
    addHeaderIcon.classList.add('addHeader');
    addHeaderIcon.setAttribute("id", annotateId);
    addHeaderIcon.setAttribute("data-target-id", annotateId);
    addHeaderIcon.style.marginRight = "10px";
    /* dropdown html */
    var ul = this._renderer.createElement("ul");
    ul.classList.add('dropdown-menu');
    ul.classList.add('header-dropdown');
    ul.setAttribute("id", "select-header-dropdown-" + annotateId);
    var rowHeaderli = this._renderer.createElement('li');
    var rowHeaderLabel = this._renderer.createElement('label');
    rowHeaderLabel.setAttribute('table-annotate-id', annotateId);
    rowHeaderLabel.classList.add('tab-header');
    rowHeaderLabel.addEventListener("click", this.tableRowHeader.bind(event));
    if (tableAnnotation.rowHeader) {
      rowHeaderLabel.innerHTML = 'First Row Header<input type="checkbox" checked="checked"><span class="checkmark" table-annotate-id="'+ annotateId +'"></span>';
    } else {
      rowHeaderLabel.innerHTML = 'First Row Header<input type="checkbox"><span class="checkmark" table-annotate-id="'+ annotateId +'"></span>';
    }
    this._renderer.appendChild(rowHeaderli, rowHeaderLabel);
    var columnHeaderli = this._renderer.createElement('li');
    var columnHeaderLabel = this._renderer.createElement('label');
    columnHeaderLabel.setAttribute('table-annotate-id', annotateId);
    columnHeaderLabel.classList.add('tab-header');
    columnHeaderLabel.addEventListener("click", this.tableColumnHeader.bind(event));
    if (tableAnnotation.columnHeader) {
      columnHeaderLabel.innerHTML = 'First Column Header<input type="checkbox" checked="checked"><span class="checkmark" table-annotate-id="'+ annotateId +'"></span>';
    } else {
      columnHeaderLabel.innerHTML = 'First Column Header<input type="checkbox"><span class="checkmark" table-annotate-id="'+ annotateId +'"></span>';
    }
    this._renderer.appendChild(columnHeaderli, columnHeaderLabel);
    this._renderer.appendChild(ul, rowHeaderli);
    this._renderer.appendChild(ul, columnHeaderli);
    this._renderer.appendChild(addHeaderIcon, ul);
    /* delete icon */
    let reject_icon = this._renderer.createElement('div');
    reject_icon.setAttribute('title', 'delete');
    reject_icon.classList.add('deleteIcon');
    reject_icon.setAttribute("id", annotateId);
    reject_icon.setAttribute("data-target-id", annotateId);
    this._renderer.appendChild(table_tools_div, addColumnIcon);
    this._renderer.appendChild(table_tools_div, addRowIcon);
    this._renderer.appendChild(table_tools_div, addHeaderIcon);
    this._renderer.appendChild(table_tools_div, reject_icon);
    addRowIcon.addEventListener("click", this.tableAddRow.bind(event));
    addColumnIcon.addEventListener("click", this.tableAddColumn.bind(event));
    reject_icon.addEventListener('click', this.handleRejectRecommendation.bind(event));
    let ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-table-tools-overlay-' + annotateId);
    if (ele) {
      ele.innerHTML = '';
      table_tools_div.style.display = 'none';
      ele.appendChild(table_tools_div);
    }
  }

  calculateRowCoordinates(e) {
    let rowAnnotationObj = {
      "uuid": UUID.UUID(),
      "class": "Annotation",
      "page": 1,
      "type": "strikeout",
      "field_type": "row",
      "color": "#FF9119",
      "fillcolor": "green",
      "rectangles": [],
      "parentUUID": ""
    }
    let annotationId = e.currentTarget.id;
    var tableAnnotation;
    for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
      if (this.PDFannotations[i].uuid === annotationId) {
        tableAnnotation = this.PDFannotations[i];
        break;
      }
    }
    var rowsList = this.PDFannotations.filter((obj: any) => {
      if (obj.parentUUID === tableAnnotation.uuid && obj.field_type === 'row') {
        return obj;
      }
    });
    rowsList = rowsList.sort((a, b) => {
      return a.rectangles[0].y - b.rectangles[0].y;
    });
    rowAnnotationObj.parentUUID = tableAnnotation.uuid;
    rowAnnotationObj.page = tableAnnotation.page;
    let coordinates = {"x": 0, "y": 0, "width": 0, "height": 0};
    coordinates.x = tableAnnotation.x;
    if (rowsList.length === 0) {
      coordinates.y = tableAnnotation.y + 30;
    } else {
      if (tableAnnotation.y + tableAnnotation.height <= rowsList[rowsList.length -1].rectangles[0].y + 30) {
        if (tableAnnotation.y + tableAnnotation.height <= rowsList[rowsList.length -1].rectangles[0].y + 10) {
          coordinates.y = tableAnnotation.y + (tableAnnotation.height)/2;
        } else {
          coordinates.y = rowsList[rowsList.length -1].rectangles[0].y + 10;
        }
      } else {
        coordinates.y = rowsList[rowsList.length -1].rectangles[0].y + 30;
      }
    }
    coordinates.width = tableAnnotation.width;
    rowAnnotationObj.rectangles.push(coordinates);
    return rowAnnotationObj;
  }

  tableRowHeader(event) {
    let annotateId = event.currentTarget.getAttribute("table-annotate-id");
    var tableAnnotation;
    for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
      if (this.PDFannotations[i].uuid === annotateId) {
        tableAnnotation = this.PDFannotations[i];
        this.PDFannotations[i].rowHeader = !this.PDFannotations[i].rowHeader;
        if (this.PDFannotations[i].rowHeader) {
          this.PDFannotations[i].columnHeader = false;
        }
        break;
      }
    }
    this.tableHeaderAdded.emit(tableAnnotation);
    this.tableToolsDivConstruct(annotateId);
  }

  tableColumnHeader(event) {
    let annotateId = event.currentTarget.getAttribute("table-annotate-id");
    var tableAnnotation;
    for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
      if (this.PDFannotations[i].uuid === annotateId) {
        tableAnnotation = this.PDFannotations[i];
        this.PDFannotations[i].columnHeader = !this.PDFannotations[i].columnHeader;
        if (this.PDFannotations[i].columnHeader) {
          this.PDFannotations[i].rowHeader = false;
        }
        break;
      }
    }
    this.tableHeaderAdded.emit(tableAnnotation);
    this.tableToolsDivConstruct(annotateId);
  }

  omrCheckboxEvent(event) {
    if (event.target.tagName === "INPUT") {
      let annotateId = event.currentTarget.id;
      var omrAnnotation;
      for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
        if (this.PDFannotations[i].uuid === annotateId) {
          omrAnnotation = this.PDFannotations[i];
          this.PDFannotations[i].isChecked = !this.PDFannotations[i].isChecked;
          break;
        }
      }
    }
  }

  tableAddRow(event) {
    let rowAnnotation = this.calculateRowCoordinates(event);
    UI.disableEdit();
    UI.enableRect("strikeout");
    PDFJSAnnotate.getStoreAdapter().addAnnotation(
      this.documentId,
      rowAnnotation.page,
      rowAnnotation
    ).then((annotation) => {
      var svgs = document.querySelectorAll(
        "svg[data-pdf-annotate-page='" + annotation.page + "']"
      );
      var viewPortDetails = svgs[0].getAttribute("data-pdf-annotate-viewport");
      var viewPortDetailsParsed = JSON.parse(viewPortDetails);
      var pp = document.createElementNS("http://www.w3.org/2000/svg", "g");
      pp.setAttribute("x",annotation.rectangles[0].x);
      pp.setAttribute("y",annotation.rectangles[0].y);
      pp.setAttribute("line-type",annotation.field_type);
      pp.setAttribute("data-pdf-annotate-id",annotation.uuid);
      pp.setAttribute("stroke-width","6");
      pp.setAttribute("data-pdf-annotate-type","strikeout");
      pp.setAttribute("stroke","#FF9119");
      pp.setAttribute("aria-hidden","true");
      var setScale = "scale("+viewPortDetailsParsed.scale+") rotate(0) translate(0, 0)";
      pp.setAttribute("transform",setScale);
      var linep = document.createElementNS("http://www.w3.org/2000/svg", "line");
      linep.setAttribute("x1",annotation.rectangles[0].x);
      linep.setAttribute("y1",annotation.rectangles[0].y);
      linep.setAttribute("x2",annotation.rectangles[0].x+annotation.rectangles[0].width);
      linep.setAttribute("y2",annotation.rectangles[0].y);
      pp.appendChild(linep);
      svgs[0].appendChild(pp);
      UI.enableEdit();
      this.annotationAdded.emit(annotation);
      this.setOrderForAnnotations();
    }, (error) => {
      console.log(error.message);
    });
  }

  setOrderForAnnotations(){
    var svgs = document.querySelectorAll("svg");
    for (var k=0; k<svgs.length; k++) {
      var allrects = svgs[k].querySelectorAll("rect");
      var tmpRects = [];
      for(var i=0; i < allrects.length;i++){
            tmpRects.push(allrects[i]);
      };
      var sortedRects = tmpRects.sort(function(rect1,rect2){
          return rect1.getAttribute("y") - rect2.getAttribute("y")
      });

      svgs[k].querySelectorAll("rect").forEach(function(rect){
          rect.remove();
      });

      for(var i=sortedRects.length-1;i>=0;i--){
          svgs[k].appendChild(sortedRects[i]);
      };
     }
  };

  calculateColumnCoordinates(e) {
    let columnAnnotationObj = {
      "uuid": UUID.UUID(),
      "class": "Annotation",
      "page": 1,
      "type": "strikeout",
      "field_type": "column",
      "color": "#FF9119",
      "fillcolor": "green",
      "rectangles": [],
      "parentUUID": ""
    }
    let annotationId = e.currentTarget.id;
    var tableAnnotation;
    for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
      if (this.PDFannotations[i].uuid === annotationId) {
        tableAnnotation = this.PDFannotations[i];
        break;
      }
    }
    var columnsList = this.PDFannotations.filter((obj: any) => {
      if (obj.parentUUID === tableAnnotation.uuid && obj.field_type === 'column') {
        return obj;
      }
    });
    columnsList = columnsList.sort((a, b) => {
      return a.rectangles[0].x - b.rectangles[0].x;
    });
    columnAnnotationObj.parentUUID = tableAnnotation.uuid;
    columnAnnotationObj.page = tableAnnotation.page;
    let coordinates = {"x": 0, "y": 0, "width": 0, "height": 0};
    if (columnsList.length === 0) {
      coordinates.x = tableAnnotation.x + 30;
    } else {
      if (tableAnnotation.x + tableAnnotation.width <= columnsList[columnsList.length -1].rectangles[0].x + 30) {
        if (tableAnnotation.x + tableAnnotation.width <= columnsList[columnsList.length -1].rectangles[0].x + 10) {
          coordinates.x = tableAnnotation.x + (tableAnnotation.width)/2;
        } else {
          coordinates.x = columnsList[columnsList.length -1].rectangles[0].x + 10;
        }
      } else {
        coordinates.x = columnsList[columnsList.length -1].rectangles[0].x + 30;
      }
    }
    coordinates.y = tableAnnotation.y;
    coordinates.height = tableAnnotation.height;
    columnAnnotationObj.rectangles.push(coordinates);
    return columnAnnotationObj;
  }

  tableAddColumn(event) {
    let columnAnnotation = this.calculateColumnCoordinates(event);
    UI.disableEdit();
    UI.enableRect("strikeout");
    PDFJSAnnotate.getStoreAdapter().addAnnotation(
      this.documentId,
      columnAnnotation.page,
      columnAnnotation
    ).then((annotation) => {
      var svgs = document.querySelectorAll(
        "svg[data-pdf-annotate-page='" + annotation.page + "']"
      );
      var viewPortDetails = svgs[0].getAttribute("data-pdf-annotate-viewport");
      var viewPortDetailsParsed = JSON.parse(viewPortDetails);
      var pp = document.createElementNS("http://www.w3.org/2000/svg", "g");
      pp.setAttribute("x",annotation.rectangles[0].x);
      pp.setAttribute("y",annotation.rectangles[0].y);
      pp.setAttribute("line-type",annotation.field_type);
      pp.setAttribute("data-pdf-annotate-id",annotation.uuid);
      pp.setAttribute("stroke-width","6");
      pp.setAttribute("data-pdf-annotate-type","strikeout");
      pp.setAttribute("stroke","#FF9119");
      pp.setAttribute("aria-hidden","true");
      var setScale = "scale("+viewPortDetailsParsed.scale+") rotate(0) translate(0, 0)";
      pp.setAttribute("transform",setScale);
      var linep = document.createElementNS("http://www.w3.org/2000/svg", "line");
      linep.setAttribute("x1",annotation.rectangles[0].x);
      linep.setAttribute("y1",annotation.rectangles[0].y);
      linep.setAttribute("x2",annotation.rectangles[0].x);
      linep.setAttribute("y2",annotation.rectangles[0].y+annotation.rectangles[0].height);
      pp.appendChild(linep);
      svgs[0].appendChild(pp);
      UI.enableEdit();
      this.annotationAdded.emit(annotation);
      this.setOrderForAnnotations();
    }, (error) => {
      console.log(error.message);
    });
  }

  tableRectMouseOver(e) {
    let annotateId;
    let ele;
    if (e.target.localName === 'div') {
      annotateId = e.target.getAttribute("data-target-id");
      ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-table-tools-overlay-' + annotateId);
    } else if (e.target.localName === 'rect') {
      annotateId = e.target.getAttribute("data-pdf-annotate-id");
      ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-table-tools-overlay-' + annotateId);
    }
    if (ele) {
      let tableToolsDiv = this._elementRef.nativeElement.querySelector('#table-tools-div-' + annotateId);
      tableToolsDiv.style.display = 'block';
    }
  }

  tableRectMouseLeave(e) {
    let annotateId;
    let ele;
    if (e.target.localName === 'div') {
      annotateId = e.target.getAttribute("data-target-id");
      ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-table-tools-overlay-' + annotateId);
    } else if (e.target.localName === 'rect') {
      annotateId = e.target.getAttribute("data-pdf-annotate-id");
      ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-table-tools-overlay-' + annotateId);
    }
    if (ele) {
      let tableToolsDiv = this._elementRef.nativeElement.querySelector('#table-tools-div-' + annotateId);
      tableToolsDiv.style.display = 'none';
      let headerSelectionDropdown = this._elementRef.nativeElement.querySelector('#select-header-dropdown-' + annotateId);
      headerSelectionDropdown.classList.remove('show');
    }
  }

  allRectMouseOver(e) {
    let annotateId;
    let ele;
    if (e.target.localName === 'div') {
      annotateId = e.target.getAttribute("data-target-id");
      ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-rect-overlay-' + annotateId);
    } else if (e.target.localName === 'rect') {
      annotateId = e.target.getAttribute("data-pdf-annotate-id");
      ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-rect-overlay-' + annotateId);
    }
    if (ele) {
      let rectToolsDiv = this._elementRef.nativeElement.querySelector('#tools-div-' + annotateId);
      rectToolsDiv.style.display = 'flex';
    }
  }

  allRectMouseLeave(e) {
    let annotateId;
    let ele;
    if (e.target.localName === 'div') {
      annotateId = e.target.getAttribute("data-target-id");
      ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-rect-overlay-' + annotateId);
    } else if (e.target.localName === 'rect') {
      annotateId = e.target.getAttribute("data-pdf-annotate-id");
      ele = this._elementRef.nativeElement.querySelector('#pdf-annotate-rect-overlay-' + annotateId);
    }
    if (ele) {
      let rectToolsDiv = this._elementRef.nativeElement.querySelector('#tools-div-' + annotateId);
      if (rectToolsDiv) {
        rectToolsDiv.style.display = 'none';
      }
    }
  }

  setRectMouseoverEvent() {
    setTimeout(() => {
      this.PDFannotations.forEach((annotation: any) => {
        var allrect = Array.from(
          document.querySelectorAll(
            "rect[data-pdf-annotate-id='" + annotation.uuid + "']"
          )
        );
        let thisVar = this;
        allrect.forEach(function (rect, i) {
          if (annotation.field_type === "table") {
            allrect[i].addEventListener("mouseover", thisVar.tableRectMouseOver.bind(event));
            allrect[i].addEventListener("mouseleave", thisVar.tableRectMouseLeave.bind(event));
            var tableToolsDiv = document.getElementById("pdf-annotate-table-tools-overlay-" + annotation.uuid);
            tableToolsDiv.addEventListener("mouseover", thisVar.tableRectMouseOver.bind(event));
            tableToolsDiv.addEventListener("mouseleave", thisVar.tableRectMouseLeave.bind(event));
          } else {
            allrect[i].addEventListener("mouseover", thisVar.allRectMouseOver.bind(event));
            allrect[i].addEventListener("mouseleave", thisVar.allRectMouseLeave.bind(event));
            var toolsDiv = document.getElementById("pdf-annotate-rect-overlay-" + annotation.uuid);
            toolsDiv.addEventListener("mouseover", thisVar.allRectMouseOver.bind(event));
            toolsDiv.addEventListener("mouseleave", thisVar.allRectMouseLeave.bind(event));
          }
        });
      });
    }, 500);
  }

  findAnnotation(documentId, annotationId) {
    let index = -1;
    let annotations = this.getAnnotations(documentId);
    for (let i = 0, l = annotations.length; i < l; i++) {
      if (annotations[i].uuid === annotationId) {
        index = i;
        break;
      }
    }
    return index;
  }
  updateAnnotations(documentId, annotations) {
    this.PDFannotations = annotations;
  }

  /* function is used to load dynamic URL's */
  public loadScript(url) {
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
  }


  renderPage(pageNumber: number) {
    UI.renderPage(pageNumber, this.PDFRenderOption).then(([pdfPage, annotations]) => {
      let viewport = pdfPage.getViewport(
        this.PDFRenderOption.scale,
        this.PDFRenderOption.rotate
      );
      this.PAGE_HEIGHT = viewport.height;

      if (this.config.useteseract) {
        var canvasavail = document.getElementById("tmpcanvas" + pageNumber);
        if (canvasavail == undefined) {
          var canvas = <HTMLCanvasElement>document.createElement('canvas');
          canvas.id = "tmpcanvas" + pageNumber;
          canvas.style.display = "none";
          var body = document.getElementById("pdfviewer");
          body.appendChild(canvas);
        }

        if (this.pageTmpCanvasLoaded[pageNumber] == undefined) {
          /* teserract code  start */
          var scale = 3;
          var viewporttmpcanvas = pdfPage.getViewport(scale);
          var canvas = <HTMLCanvasElement>document.getElementById("tmpcanvas" + pageNumber);
          canvas.height = viewporttmpcanvas.height;
          canvas.width = viewporttmpcanvas.width;
          var context = canvas.getContext('2d');
          var renderContext = {
            canvasContext: context,
            viewport: viewporttmpcanvas
          };
          pdfPage.render(renderContext);
          this.pageTmpCanvasLoaded[pageNumber] = true;
          /* teserract code  end */
        }
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    const docId: SimpleChange = changes.documentId;
  }

  configChanged(changes: KeyValueChanges<string, any>) {
    if (changes["_changesHead"] && changes["_changesHead"]["key"] == "zoomLevel") {
      this.PDFRenderOption.scale = changes["_changesHead"]["currentValue"];
      PDFJS.getDocument(this.PDFRenderOption.documentId).then(
        pdf => {
          this.PDFRenderOption.pdfDocument = pdf;
          this.PDFInfo.totalPages = this.PDFRenderOption.pdfDocument.pdfInfo.numPages;
          this.config.totalpages = this.PDFInfo.totalPages;
          let viewer = document.getElementById('viewer');
          viewer.innerHTML = '';
          for (let i = 0; i < this.PDFInfo.totalPages; i++) {
            let page = UI.createPage(i + 1);
            viewer.appendChild(page);
          }
          this.pageLoaded = {};
          this.pageLoaded[this.PDFInfo.currentPage] = true;
          if (this.PDFInfo.totalPages >= this.PDFInfo.currentPage+1) {
            this.pageLoaded[this.PDFInfo.currentPage+1] = true;
          }
          if (this.PDFInfo.currentPage-1 >= 1) {
            this.pageLoaded[this.PDFInfo.currentPage-1] = true;
          }

          let loadedPageskeys = Object.keys(this.pageLoaded);

          for (let i = 0; i < loadedPageskeys.length; i++) {
            let pageNumber = parseInt(loadedPageskeys[i]);
            this.renderPage(pageNumber);
          }
          UI.disableEdit();
          UI.enableRect("strikeout");
          setTimeout(() => {
              UI.enableRect('area');
              UI.enableEdit();
              this.setOrderForAnnotations();
          },1000)
        },
        error => {
          console.log('Error', error);
        }
      );

      //this.renderPdf();
    } else if (changes["_changesHead"] && changes["_changesHead"]["key"] == "currentPage") {
      let scalerender = this.PDFRenderOption.scale * 100;
      let scaleheight = (scalerender) / 100;
      this.PDFInfo.currentPage = changes["_changesHead"]["currentValue"];
      let visiblePage = document.querySelector(
        `.page[data-page-number="${
        this.PDFInfo.currentPage
        }"][data-loaded="false"]`
      );
      if (visiblePage) {
        UI.renderPage(this.PDFInfo.currentPage, this.PDFRenderOption);
        setTimeout(() => {
          if (this.config.rendercirclesalongwithindex) {
            this.renderCircles();
          }
          document.getElementById('pdfviewer').scrollTop =
            (this.PDFInfo.currentPage - 1) * this.PAGE_HEIGHT + scaleheight - 15;
        }, 1500);
      } else {
        document.getElementById('pdfviewer').scrollTop =
        (this.PDFInfo.currentPage - 1) * this.PAGE_HEIGHT + scaleheight - 15;
      }
    } else if (changes["_changesHead"] && changes["_changesHead"]["key"] == "showLabels") {
      this.config.showLabels = changes["_changesHead"]["currentValue"];
      if (this.config.showLabels) {
        var allFieldLabels = Array.from(document.querySelectorAll(".keyValueLabelRep"));
        var allLabels = Array.from(document.querySelectorAll(".annotationLabelRep"));
        allFieldLabels.forEach(function (label, i) {
          allFieldLabels[i].classList.remove('displayNone');
          allFieldLabels[i].classList.add('displayBlock');
        });
        allLabels.forEach(function (label, i) {
          allLabels[i].classList.remove('displayNone');
          allLabels[i].classList.add('displayBlock');
        });
      } else {
        var allFieldLabels = Array.from(document.querySelectorAll(".keyValueLabelRep"));
        var allLabels = Array.from(document.querySelectorAll(".annotationLabelRep"));
        allFieldLabels.forEach(function (label, i) {
          allFieldLabels[i].classList.remove('displayBlock');
          allFieldLabels[i].classList.add('displayNone');
        });
        allLabels.forEach(function (label, i) {
          allLabels[i].classList.remove('displayBlock');
          allLabels[i].classList.add('displayNone');
        });
      }
    }
  }

  ngDoCheck(): void {
    const changes = this.configDiffer.diff(this.config);
    if (changes) {
      this.configChanged(changes);
    }
  }

  /* function use for scroll to the particular rect position */

  setScroll(annotation) {
    if (annotation == undefined) return;
    let scalerender = this.PDFRenderOption.scale * 100;
    let scaleheight = (scalerender * annotation.y) / 100;
    let scalewidth = (scalerender * annotation.x) / 100;
    this.PDFInfo.currentPage = annotation.page;
    let visiblePage = document.querySelector(
      `.page[data-page-number="${
      this.PDFInfo.currentPage
      }"][data-loaded="false"]`
    );
    if (visiblePage) {
      UI.renderPage(this.PDFInfo.currentPage, this.PDFRenderOption);
      setTimeout(() => {
        if (this.config.rendercirclesalongwithindex) {
          this.renderCircles();
        }
        document.getElementById('pdfviewer').scrollTop =
          (parseInt(annotation.page) - 1) * this.PAGE_HEIGHT + scaleheight - 15;
        // document.getElementById('pdfviewer').scrollLeft = scalewidth + 5;
        var allrect = Array.from(
          document.querySelectorAll(
            "rect[data-pdf-annotate-id='" + annotation.uuid + "']"
          )
        );
        allrect.forEach(function (rect) {
          rect.classList.add('active');
        });
      }, 1500);
    } else {
      if (this.config.useteseract) {
        //this.renderPDFForeTeserract(annotation);
      }
      document.getElementById('pdfviewer').scrollTop =
        (parseInt(annotation.page) - 1) * this.PAGE_HEIGHT + scaleheight - 15;
      //document.getElementById('pdfviewer').scrollLeft = scalewidth + 5;
      var allrect = Array.from(
        document.querySelectorAll(
          "rect[data-pdf-annotate-id='" + annotation.uuid + "']"
        )
      );
      allrect.forEach(function (rect) {
        rect.classList.add('active');
      });
    }
  }

  renderPDFForeTeserract(annotation: any) {
    UI.renderPage(annotation.page, this.PDFRenderOption).then(([pdfPage, annotations]) => {
      /* teserract code  start */
      var scale = 3;
      var viewporttmpcanvas = pdfPage.getViewport(scale);
      var canvas = <HTMLCanvasElement>document.getElementById("tmpCanvas");
      canvas.height = viewporttmpcanvas.height;
      canvas.width = viewporttmpcanvas.width;
      var context = canvas.getContext('2d');
      var renderContext = {
        canvasContext: context,
        viewport: viewporttmpcanvas
      };
      pdfPage.render(renderContext);
      /* teserract code  end */

    });
  }

  /* reset the active rect */
  resetActiveRectClass() {
    var allrect = Array.from(document.querySelectorAll("rect[class='active']"));
    allrect.forEach(function (rect) {
      rect.classList.remove('active');
    });
    this.setRecommendationIcons();
  }

  highlightAnnotationEvent(uuid: string) {
    if (this.config.deleteAlert) {
      if (this._elementRef.nativeElement.querySelector("#annotate-delete-overlay-done")) {
        document.getElementById("annotate-delete-overlay-done").classList.add("disableDelete");
      }
    }
    let annotationIndex = this.findAnnotation(this.documentId, uuid);
    this.resetActiveRectClass();
    this.setScroll(this.PDFannotations[annotationIndex]);
  }

  editAnnotationEvent(uuid: string) {
    let annotationIndex = this.findAnnotation(this.documentId, uuid);
    if (annotationIndex == -1)
      return;
    if (this.config.history_enabled) {
      this.addannotationtoHistory(this.PDFannotations[annotationIndex]);
    }
    this.resetActiveRectClass();
    if (this.config.history_enabled) {
      this.editHistoryLength = (this.annotationHistory && this.annotationHistory[uuid]) ? this.annotationHistory[uuid].length : '0';
    }
    // this.setScroll(this.PDFannotations[annotationIndex]);
    var target = Array.from(
      document.querySelectorAll(
        "rect[data-pdf-annotate-id='" + uuid + "']"
      )
    );
    /* enable annotation id to edit */
    target.forEach(function (rect) {
      if (rect["style"]["display"] == "none") {
        rect["style"]["display"] = "block";
      }
    });
    setTimeout(() => {
      UI.enableEdit();
      this.editannotationuuid = uuid;
      this.editAnnotation(uuid);
    }, 200);

  }

  addAnnotationEvent(obj: any) {
    this.eventName = "AddEvent";
    this.newannotationuuid = obj.uuid;
    this.newannotationObj = obj;
    UI.disableEdit();
    document.getElementById("pdfviewer").classList.add("cropping");
    UI.enableRect("area");
    UI.enableEdit();
    this.rectHideandShow('transparent');
  }

  /* function use for edit annotation */
  editAnnotation(uuid: string) {
    document.getElementById('pdfviewer').classList.remove('cropping');
    // UI.disableRect();
    var target = Array.from(
      document.querySelectorAll(
        "rect[data-pdf-annotate-id='" + uuid + "']"
      )
    );
    /* enable annotation id to edit */
    target.forEach(function (rect) {
      if (rect["style"]["display"] === "none") {
        rect["style"]["display"] = "block";
      }
    });

    UI.fireEvent('annotation:click', target[0]);
    // setTimeout(() => {
    //   this.rectHideandShow('transparent');
    // }, 400);
  }

  /* event fire when click on annotation */
  handleAnnotationClick(target) {
    if (target) {
      let annotationId = target.attributes["data-pdf-annotate-id"].textContent;
      let annotation;
      for (var i = 0, l = this.PDFannotations.length; i < l; i++) {
        if (this.PDFannotations[i].uuid === annotationId) {
          annotation = this.PDFannotations[i];
          break;
        }
      }
      this.selectedAnnotation = cloneDeep(annotation);
      if (this.config.deleteAlert && (this._elementRef.nativeElement.querySelector("#annotate-delete-overlay-done"))) {
        document.getElementById("annotate-delete-overlay-done").classList.add("activeDelete");
        document.getElementById("annotate-delete-overlay-done").classList.remove("disableDelete");
      }
      UI.disableRect();
      setTimeout(() => {
        if (this._elementRef.nativeElement.querySelector("#pdf-annotate-edit-overlay")) {
          this._renderer.appendChild(this._elementRef.nativeElement.querySelector("#pdf-annotate-edit-overlay"), this.child_div);
        }
        if (this.config.history_enabled) {
          setTimeout(() => {
            if (this._elementRef.nativeElement.querySelector("#undo_span_id"))
              this._elementRef.nativeElement.querySelector("#undo_span_id").innerText = "Undo (" + (this.editHistoryLength === "1" ? "0" : (parseInt(this.editHistoryLength) - 1).toString()) + ")";
          }, 500);
        }
      }, 500);
    }
  }

  /* used the below funtion to hide rect when adding new annotation */
  rectHideandShow(colortype) {
    var allrect = Array.from(document.querySelectorAll("rect"));
    allrect.forEach(function (rect) {
      if (rect.getAttribute("data-pdf-annotate-id") != this.editannotationuuid) {
        if (colortype == "transparent") {
          rect.style.display = "none";
        }
        else {
          rect.style.display = "block";
        }
      }
    }.bind(this));
  }

  addannotationtoHistory(annotation) {
    var anno = {};
    var target = Array.from(
      document.querySelectorAll(
        "rect[data-pdf-annotate-id='" + annotation.uuid + "']"
      )
    );
    Object.assign(anno, annotation);
    if (this.annotationHistory[annotation.uuid]) {
      var is_there_annotation = false;
      for (var i = 0; i < this.annotationHistory[annotation.uuid].length; i++) {
        if (JSON.stringify(this.annotationHistory[annotation.uuid][i].annotation) === JSON.stringify(annotation)) {
          is_there_annotation = true;
          break;
        }
      }
      if (is_there_annotation == false && this.annotationHistory[annotation.uuid].length < this.config.history_count_per_annotation)
        this.annotationHistory[annotation.uuid].push({ "annotation": anno, "target": target[0] });
    } else {
      this.annotationHistory[annotation.uuid] = [];
      this.annotationHistory[annotation.uuid].push({ "annotation": anno, "target": target[0] });
    }
  }

  handleUndo() {
    this.eventName = "undoEvent";
    let alength = this.annotationHistory[this.editannotationuuid].length - 2;
    if (alength < 0)
      return;
    let old_annotation = this.annotationHistory[this.editannotationuuid][alength].annotation;
    let old_target = this.annotationHistory[this.editannotationuuid][alength].target;
    let rect: any = Array.from(
      document.querySelectorAll(
        "rect[data-pdf-annotate-id='" + old_annotation.uuid + "']"
      )
    )[0];
    rect.setAttribute("x", old_target.getAttribute("x"));
    rect.setAttribute("y", old_target.getAttribute("y"));
    rect.setAttribute("width", old_target.getAttribute("width"));
    rect.setAttribute("height", old_target.getAttribute("height"));
    this.eventName = "";
    PDFJSAnnotate.__storeAdapter.editAnnotation(this.documentId, this.annotationHistory[this.editannotationuuid][alength].annotation.uuid, this.annotationHistory[this.editannotationuuid][alength].annotation);
    var p = this.annotationHistory[this.editannotationuuid];
    this.annotationHistory[this.editannotationuuid] = [];
    if (p.length == 2) {
      Object.assign(p, []);
    } else {
      let s = p.splice(this.annotationHistory[this.editannotationuuid].length - 1, 1);
      this.annotationHistory[this.editannotationuuid] = p;
    }
  }

  filterAnnotationsbyStatus(statusFilter: Array<Filters>) {
    if (statusFilter.length == 0) {
      this.rectHideandShow("color");
      return;
    }

    let queryFilterString = "";
    let queryFilterString_hidden = "";
    setTimeout(() => {
      /* matched nodes showing */
      statusFilter.forEach(element => {
        queryFilterString = queryFilterString + "rect[status='" + element + "'],";
      });
      queryFilterString = queryFilterString.substr(0, queryFilterString.length - 1);
      let filter_nodes = Array.from(document.querySelector('.page[data-page-number="' + this.PDFInfo.currentPage + '"]').querySelectorAll(queryFilterString));
      filter_nodes.forEach(function (rect) {
        rect["style"]["display"] = "block";
      });

      /* unmatched nodes hidden */
      statusFilter.forEach(element => {
        queryFilterString_hidden = queryFilterString_hidden + "rect:not([status='" + element + "']),";
      });
      queryFilterString_hidden = queryFilterString_hidden.substr(0, queryFilterString_hidden.length - 1);
      let filter_nodes_hidden = Array.from(document.querySelector('.page[data-page-number="' + this.PDFInfo.currentPage + '"]').querySelectorAll(queryFilterString_hidden));
      filter_nodes_hidden.forEach(function (rect) {
        rect["style"]["display"] = "none";
      });
    }, 0);
  }


}

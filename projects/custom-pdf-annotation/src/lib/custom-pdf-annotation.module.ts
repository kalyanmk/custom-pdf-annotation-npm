import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CustomPdfAnnotationComponent } from './custom-pdf-annotation.component';

@NgModule({
  declarations: [CustomPdfAnnotationComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [CustomPdfAnnotationComponent]
})
export class CustomPdfAnnotationModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomPdfAnnotationComponent } from './custom-pdf-annotation.component';

describe('CustomPdfAnnotationComponent', () => {
  let component: CustomPdfAnnotationComponent;
  let fixture: ComponentFixture<CustomPdfAnnotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomPdfAnnotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomPdfAnnotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

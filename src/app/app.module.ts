import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { CustomPdfAnnotationModule } from 'custom-pdf-annotation';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CustomPdfAnnotationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

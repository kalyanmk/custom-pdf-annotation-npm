import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { CustomPdfAnnotationComponent, PDFconfig } from 'custom-pdf-annotation';
import { pdf_annotations, rowsAndColumns } from 'src/app/config/pdf-annotations.config';
import cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'custom-pdf-annotation-lib';
  documentId: string;
  PDF_annotations:any = [];
  PDFConfig: PDFconfig = {
    "loadallpages": false, "rendercirclesalongwithindex": false,
    "rotate": "0", "zoomLevel": 1, "totalpages": 1, "statusFilter": [], "deleteAlert": true, "useteseract": true, "currentPage": 1, "showLabels": true
  };

  @ViewChild(CustomPdfAnnotationComponent)
  private annotationcomponent: CustomPdfAnnotationComponent;

  constructor(private elem: ElementRef) {

  }


  ngOnInit() {
    this.documentId = 'assets/src.pdf';
    // this.PDF_annotations = pdf_annotations;
    this.PDF_annotations = [];
    setTimeout(() => {
      this.addRegion('heading');
    }, 200);
  }

  addRegion(type: string) {
    this.annotationcomponent.setAnnotationMode({uuid: '', field_type: type});
  }

  editRegion() {
    let id = "cf917d5a-d1ba-4978-d3de-f02073f0dee9";
    this.annotationcomponent.editAnnotationEvent(id);
  }

  mouseLeaveFrmPDF(event) {

  }

  onRecommendationAccept(event) {
    console.log(event.annotationId);
  }

  onRecommendationRejected(event) {
    console.log(event.annotationId);
  }

  annotationAdded(event) {
    console.log(event);
    console.log(this.PDF_annotations);
    // if (event.field_type === 'table') {
    //   rowsAndColumns.forEach(element => {
    //     element.parentUUID = event.uuid;
    //   });
    //   this.PDF_annotations = this.PDF_annotations.concat(rowsAndColumns);
    //   this.PDFConfig.zoomLevel = this.PDFConfig.zoomLevel + 0.00001;
    // }
  }

  modifyAnnotations() {
    this.PDF_annotations = pdf_annotations;
  }

  tableHeaderAdded(event) {
    console.log(event);
  }

  endOfPdfloading(event) {
    // if (event) {
    //   this.annotationcomponent.setAnnotationMode({uuid: '', field_type: 'section'});
    // }
  }

  annotationUpdated(event) {
    console.log(event);
  }

  annotationDeleted(event) {
    console.log(event);
  }

  captureData(event) {
    
  }

  onPageChange(event) {

  }

  zoomIn() {
    this.PDFConfig.zoomLevel = this.PDFConfig.zoomLevel + 0.2;
  }
  zoomOut() {
    this.PDFConfig.zoomLevel = this.PDFConfig.zoomLevel - 0.2;
  }

  showLabels() {
    this.PDFConfig.showLabels = !this.PDFConfig.showLabels;
  }
}

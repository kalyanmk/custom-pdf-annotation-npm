export const pdf_annotations = [
  {
    "type": "area",
    "x": 288,
    "y": 2207,
    "width": 1955,
    "height": 606,
    "class": "Annotation",
    "uuid": "8ac0bd69-777e-1f4b-bd01-568c80eab579",
    "element_id": "ele_bb172769-7519-2471-6358-90cd8fbde9dd",
    "element_count": 1,
    "page": 1,
    "field_type": "table",
    "accepted": true,
    "documentId": "assets/src.pdf"
  },
  {
    "uuid": "f31b00db-5aab-fc05-142b-151d40aae40f",
    "class": "Annotation",
    "page": 1,
    "type": "strikeout",
    "field_type": "column",
    "color": "#FF9119",
    "fillcolor": "green",
    "rectangles": [
      {
        "x": 876.878787878788,
        "y": 2207,
        "width": 0,
        "height": 606
      }
    ],
    "parentUUID": "8ac0bd69-777e-1f4b-bd01-568c80eab579",
    "accepted": true,
    "documentId": "assets/src.pdf"
  },
  {
    "uuid": "b8bb4885-2599-bfd0-d357-925335d69b50",
    "class": "Annotation",
    "page": 1,
    "type": "strikeout",
    "field_type": "row",
    "color": "#FF9119",
    "fillcolor": "green",
    "rectangles": [
      {
        "x": 288,
        "y": 2274.529411764706,
        "width": 1955,
        "height": 0
      }
    ],
    "parentUUID": "8ac0bd69-777e-1f4b-bd01-568c80eab579",
    "accepted": true,
    "documentId": "assets/src.pdf"
  },
  {
    "uuid": "9d8ca042-e918-d4f2-1b77-e37f2bc711db",
    "class": "Annotation",
    "page": 1,
    "type": "strikeout",
    "field_type": "row",
    "color": "#FF9119",
    "fillcolor": "green",
    "rectangles": [
      {
        "x": 288,
        "y": 2471.5882352941176,
        "width": 1955,
        "height": 0
      }
    ],
    "parentUUID": "8ac0bd69-777e-1f4b-bd01-568c80eab579",
    "accepted": true,
    "documentId": "assets/src.pdf"
  },
  {
    "uuid": "a25489b7-ed24-69c1-3bf7-eab715583504",
    "class": "Annotation",
    "page": 1,
    "type": "strikeout",
    "field_type": "row",
    "color": "#FF9119",
    "fillcolor": "green",
    "rectangles": [
      {
        "x": 288,
        "y": 2703.941176470588,
        "width": 1955,
        "height": 0
      }
    ],
    "parentUUID": "8ac0bd69-777e-1f4b-bd01-568c80eab579",
    "accepted": true,
    "documentId": "assets/src.pdf"
  }
];

export const rowsAndColumns = [
  {
    "uuid": "UUId0.01",
    "class": "Annotation",
    "page": 1,
    "type": "strikeout",
    "field_type": "row",
    "color": "#f4911c",
    "fillcolor": "green",
    "rectangles": [
      {
        "x": 291,
        "y": 2283,
        "width": 1960,
        "height": 0
      }
    ],
    "parentUUID": "cf917d5a-d1ba-4978-d3de-f02073table848",
    "documentId": "assets/src.pdf"
  },
  {
    "uuid": "UUId0.70",
    "class": "Annotation",
    "page": 1,
    "type": "strikeout",
    "field_type": "row",
    "color": "#f4911c",
    "fillcolor": "green",
    "rectangles": [
      {
        "x": 291,
        "y": 2471.8888888888887,
        "width": 1960,
        "height": 0
      }
    ],
    "parentUUID": "cf917d5a-d1ba-4978-d3de-f02073table848",
    "documentId": "assets/src.pdf"
  },
  {
    "uuid": "UUId0.18",
    "class": "Annotation",
    "page": 1,
    "type": "strikeout",
    "field_type": "row",
    "color": "#f4911c",
    "fillcolor": "green",
    "rectangles": [
      {
        "x": 291,
        "y": 2707.4444444444443,
        "width": 1960,
        "height": 0
      }
    ],
    "parentUUID": "cf917d5a-d1ba-4978-d3de-f02073table848",
    "documentId": "assets/src.pdf"
  },
  {
    "uuid": "UUId0.79",
    "class": "Annotation",
    "page": 1,
    "type": "strikeout",
    "field_type": "column",
    "color": "#f4911c",
    "fillcolor": "green",
    "rectangles": [
      {
        "x": 887.4444444444445,
        "y": 2206,
        "width": 0,
        "height": 604
      }
    ],
    "parentUUID": "cf917d5a-d1ba-4978-d3de-f02073table848",
    "documentId": "assets/src.pdf"
  }
];
